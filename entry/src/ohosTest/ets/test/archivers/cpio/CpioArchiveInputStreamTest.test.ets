import fileio from '@ohos.fileio';
import featureAbility from '@ohos.ability.featureAbility';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from "hypium/index";
import { CpioArchiveInputStream, InputStream, OutputStream, ArchiveOutputStream, ArchiveStreamFactory,
  CpioArchiveEntry, StringBuilder, IOUtils, File, ArchiveEntry, Long } from '@ohos/commons-compress';
/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export default function CpioArchiveInputStreamTest() {
  describe('cpioArchiveInputStreamTest', function () {

    it('cpioArchiveInputStreamTest1', 0, function (done) {
      var context = featureAbility.getContext();
      context.getCacheDir()
        .then((data) => {
          testCpioUnarchive(data)
          done()
        })
        .catch((error) => {
          console.error('File to obtain the file directory. Cause: ' + error.message);
        })
    })

    it('cpioArchiveInputStreamTest2', 0, function (done) {
      var context = featureAbility.getContext();
      context.getCacheDir()
        .then((data) => {
          testCpioUnarchiveCreatedByRedlineRpm(data)
          done()
        })
        .catch((error) => {
          console.error('File to obtain the file directory. Cause: ' + error.message);
        })
    })


    it('cpioArchiveInputStreamTest4', 0, function (done) {
      var context = featureAbility.getContext();
      context.getCacheDir()
        .then((data) => {
          singleByteReadConsistentlyReturnsMinusOneAtEof(data)
          done()
        })
        .catch((error) => {
          console.error('File to obtain the file directory. Cause: ' + error.message);
        })
    })

    it('cpioArchiveInputStreamTest4', 0, function (done) {
      var context = featureAbility.getContext();
      context.getCacheDir()
        .then((data) => {
          multiByteReadConsistentlyReturnsMinusOneAtEof(data)
          done()
        })
        .catch((error) => {
          console.error('File to obtain the file directory. Cause: ' + error.message);
        })
    })
  })
}


function testCpioUnarchive(data): void {

  const writer = fileio.openSync(data + '/test1.xml', 0o102, 0o666);
  fileio.writeSync(writer, "<?xml version = '1.0'?>\n"
  + "<!DOCTYPE connections>\n"
  + "<connections>\n"
  + "</connections>");
  fileio.closeSync(writer);


  let output: File = new File(data, "bla.cpio");
  let file1: File = new File(data, "test1.xml");
  let inputStream1 = new InputStream();
  inputStream1.setFilePath(file1.getPath());
  let out: OutputStream = new OutputStream();
  out.setFilePath(output.getPath());
  let os: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStreamLittle("cpio", out);
  let archiveEntry1: CpioArchiveEntry = new CpioArchiveEntry();
  archiveEntry1.initCpioArchiveEntryNameSize("test1.xml", Long.fromNumber(file1.length()));
  os.putArchiveEntry(archiveEntry1);
  IOUtils.copy(inputStream1, os);
  os.closeArchiveEntry();

  os.close();
  out.close();


  let expected: StringBuilder = new StringBuilder();
  expected.append("./test1.xml<?xml version=\"1.0\"?>\n");
  expected.append("<empty/>./test2.xml<?xml version=\"1.0\"?>\n");
  expected.append("<empty/>\n");
  let path = "/data/data/com.mykey.myapp/cache/test.xml";


  let result: StringBuilder = new StringBuilder();
  let inFile: File = new File(data, "bla.cpio");
  let input: InputStream = new InputStream();
  input.setFilePath(inFile.getPath());
  let s: CpioArchiveInputStream = new CpioArchiveInputStream(input, 512, "US-ASCII");

  let entry: CpioArchiveEntry;

  while ((entry = s.getNextEntry()) != null) {
    result.append(entry.getName());
    let tmp: number;
    while ((tmp = s.read()) != -1) {
      result.append(tmp);
    }
  }
  expect(expected.toString() == result.toString())

}


function testCpioUnarchiveCreatedByRedlineRpm(data) {

  const writer = fileio.openSync(data + '/test1.txt', 0o102, 0o666);
  fileio.writeSync(writer, "Hello World!");
  fileio.closeSync(writer);

  let output: File = new File(data, "redline.cpio");
  let file1: File = new File(data, "test1.txt");
  let inputStream1 = new InputStream();
  inputStream1.setFilePath(file1.getPath());
  let out: OutputStream = new OutputStream();
  out.setFilePath(output.getPath());
  let os: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStreamLittle("cpio", out);
  let archiveEntry1: CpioArchiveEntry = new CpioArchiveEntry();
  archiveEntry1.initCpioArchiveEntryNameSize("test1.txt", Long.fromNumber(file1.length()));
  os.putArchiveEntry(archiveEntry1);
  IOUtils.copy(inputStream1, os);
  os.closeArchiveEntry();

  os.close();
  out.close();


  let count: number = 0;
  let inFile: File = new File(data, "redline.cpio");
  let input: InputStream = new InputStream();
  input.setFilePath(inFile.getPath());
  let s: CpioArchiveInputStream = new CpioArchiveInputStream(input, 512, "US-ASCII");

  let entry: CpioArchiveEntry = null;

  while ((entry = s.getNextEntry()) != null) {
    count++;

  }

  expect(count).assertEqual(1)

}


function singleByteReadConsistentlyReturnsMinusOneAtEof(data) {
  const writer = fileio.openSync(data + '/test1.xml', 0o102, 0o666);
  fileio.writeSync(writer, "<?xml version = '1.0'?>\n"
  + "<!DOCTYPE connections>\n"
  + "<connections>\n"
  + "</connections>");
  fileio.closeSync(writer);


  let output: File = new File(data, "bla.cpio");
  let file1: File = new File(data, "test1.xml");
  let inputStream1 = new InputStream();
  inputStream1.setFilePath(file1.getPath());
  let out: OutputStream = new OutputStream();
  out.setFilePath(output.getPath());
  let os: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStreamLittle("cpio", out);
  let archiveEntry1: CpioArchiveEntry = new CpioArchiveEntry();
  archiveEntry1.initCpioArchiveEntryNameSize("test1.xml", Long.fromNumber(file1.length()));
  os.putArchiveEntry(archiveEntry1);
  IOUtils.copy(inputStream1, os);
  os.closeArchiveEntry();

  os.close();
  out.close();

  let inFile: File = new File(data, "bla.cpio");
  let input: InputStream = new InputStream();
  input.setFilePath(inFile.getPath());
  let s: CpioArchiveInputStream = new CpioArchiveInputStream(input, 512, "US-ASCII");
  let e: ArchiveEntry = s.getNextEntry();

  IOUtils.toByteArray(s);

  expect(s.read()).assertEqual(-1)

  expect(s.read()).assertEqual(-1)
}


function multiByteReadConsistentlyReturnsMinusOneAtEof(data) {
  const writer = fileio.openSync(data + '/test1.xml', 0o102, 0o666);
  fileio.writeSync(writer, "<?xml version = '1.0'?>\n"
  + "<!DOCTYPE connections>\n"
  + "<connections>\n"
  + "</connections>");
  fileio.closeSync(writer);


  let output: File = new File(data, "bla.cpio");
  let file1: File = new File(data, "test1.xml");
  let inputStream1 = new InputStream();
  inputStream1.setFilePath(file1.getPath());
  let out: OutputStream = new OutputStream();
  out.setFilePath(output.getPath());
  let os: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStreamLittle("cpio", out);
  let archiveEntry1: CpioArchiveEntry = new CpioArchiveEntry();
  archiveEntry1.initCpioArchiveEntryNameSize("test1.xml", Long.fromNumber(file1.length()));
  os.putArchiveEntry(archiveEntry1);
  IOUtils.copy(inputStream1, os);
  os.closeArchiveEntry();

  os.close();
  out.close();


  let buf: Int8Array = new Int8Array(2);
  let inFile: File = new File(data, "bla.cpio");
  let input: InputStream = new InputStream();
  input.setFilePath(inFile.getPath());
  let s: CpioArchiveInputStream = new CpioArchiveInputStream(input, 512, "US-ASCII");
  let e: ArchiveEntry = s.getNextEntry();
  IOUtils.toByteArray(s);

  expect(s.readBytes(buf)).assertEqual(-1)
  expect(s.readBytes(buf)).assertEqual(-1)
}



     



