/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { lz4Compressed, lz4Decompressed, System } from '@ohos/commons-compress';
import fileio from '@ohos.fileio';

@Entry
@Component
export struct Lz4Test {
  TAG: string = 'lz4Test----'
  @State isCompressLz4Show: boolean = false;
  @State isDeCompressLz4Show: boolean = false;
  preTimestamp: number = 0;

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Text('lz4相关功能')
        .fontSize(20)
        .margin({ top: 16 })

      Text('点击生成test.txt')
        .fontSize(16)
        .margin({ top: 32 })
        .padding(8)
        .border({ width: 2, color: '#535353', radius: 6 })
        .onClick((event) => {
          if (!this.isFastClick()) {
            this.generateTextFile()
          }
        })

      if (true) {
        Text('点击压缩test.txt为lz4文件')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.lz4CompressedTest()
            }
          })
      }

      if (true) {
        Text('点击解压lz4文件')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.lz4DecompressedTest()
            }
          })
      }
    }
    .width('100%')
    .height('100%')
  }

  isFastClick(): boolean {
    var timestamp = Date.parse(new Date().toString());
    if ((timestamp - this.preTimestamp) > 1500) {
      this.preTimestamp = timestamp;
      return false;
    } else {
      return true;
    }
  }

  generateTextFile(): void {
    try{
      var data = globalThis.context.filesDir
        const writer = fileio.openSync(data + '/test.txt', 0o102, 0o666);
        fileio.writeSync(writer, "hello, 臭儿子!  adjasjdakjdakjdkjakjdakjskjasdkjaskjdajksdkjasdkjaksjdkja\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs"
        );
        fileio.closeSync(writer);
        AlertDialog.show({ title: '生成成功',
          message: '请查看手机路径' + data + '/test.txt',
          confirm: { value: 'OK', action: () => {
            this.isCompressLz4Show = true
          } }
        })
      }catch(error){
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  lz4CompressedTest(): void {
    try{
      var data = globalThis.context.filesDir
        let timer1 = System.currentTimeMillis().toString()
        console.info(this.TAG + timer1)

        lz4Compressed(data + '/bla.tar', data + '/bla.tar.lz4')

        let timer2: string = System.currentTimeMillis().toString()
        console.info(this.TAG + timer2)

        AlertDialog.show({ title: '压缩成功',
          message: '请查看手机路径 ' + data + '/test.txt.lz4',
          confirm: { value: 'OK', action: () => {
            this.isDeCompressLz4Show = true
          } }
        })
      }catch(error){
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  lz4DecompressedTest(): void {
    try{
      var data = globalThis.context.filesDir

        let timer1 = System.currentTimeMillis().toString()
        console.info(this.TAG + timer1)

        lz4Decompressed(data + '/bla.tar.lz4-framed.lz4', data + '/bla.tar')

        let timer2: string = System.currentTimeMillis().toString()
        console.info(this.TAG + timer2)

        AlertDialog.show({ title: '解缩成功',
          message: '请查看手机路径 ' + data + '/test2.txt',
          confirm: { value: 'OK', action: () => {
          } }
        })
      }catch(error){
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }
}