/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Unsafe from './Unsafe';
import ZstdFrameDecompressor from './ZstdFrameDecompressor'
import Decompressor from './Decompressor'
import Long from '../util/long/index'

export default class ZstdDecompressor implements Decompressor {
    private decompressor: ZstdFrameDecompressor = new ZstdFrameDecompressor();

    public decompress(input: Int8Array, inputOffset: number, inputLength: number, output: Int8Array, outputOffset: number, maxOutputLength: number): number {
        let inputAddress: Long = Long.fromNumber(Unsafe.ARRAY_BYTE_BASE_OFFSET + inputOffset);
        let inputLimit: Long = inputAddress.add(inputLength);
        let outputAddress: Long = Long.fromNumber(Unsafe.ARRAY_BYTE_BASE_OFFSET + outputOffset);
        let outputLimit: Long = outputAddress.add(maxOutputLength);

        return this.decompressor.decompress(input, inputAddress, inputLimit, output, outputAddress, outputLimit);
    }

    public static getDecompressedSize(input: Int8Array, offset: number, length: number): Long {
        let baseAddress: number = Unsafe.ARRAY_BYTE_BASE_OFFSET + offset;
        return ZstdFrameDecompressor.getDecompressedSize(input, Long.fromNumber(baseAddress), Long.fromNumber(baseAddress + length));
    }
}
