/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Arrays from '../util/Arrays';
import NodeTable from './NodeTable'
import Huffman from './Huffman'

export default class HuffmanCompressionTableWorkspace {
    public nodeTable: NodeTable = new NodeTable((2 * Huffman.MAX_SYMBOL_COUNT - 1)); // number of nodes in binary tree with MAX_SYMBOL_COUNT leaves

    public entriesPerRank: Int16Array = new Int16Array(Huffman.MAX_TABLE_LOG + 1);
    public valuesPerRank: Int16Array = new Int16Array(Huffman.MAX_TABLE_LOG + 1);
    public rankLast: Int32Array= new Int32Array(Huffman.MAX_TABLE_LOG + 2);

    public reset(): void
    {
        Arrays.fill(this.entriesPerRank, 0);
        Arrays.fill(this.valuesPerRank, 0);
    }
}
