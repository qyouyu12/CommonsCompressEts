/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * xz
 */
export { default as XZCompressorInputStream } from './components/compressors/xz/XZCompressorInputStream'

export { default as XZCompressorOutputStream } from './components/compressors/xz/XZCompressorOutputStream'

/**
 * z
 */
export { default as ZCompressorInputStream } from './components/compressors/z/ZCompressorInputStream'

/**
 * zip
 */
export { zipDeCompress, zipCompress } from './components/zip/zipApi'

/**
 * zstd
 */
export { default as ZstdDecompressor } from './components/zstd/ZstdDecompressor'

export { default as ZstdCompressor } from './components/zstd/ZstdCompressor'

export { default as FrameHeader } from './components/zstd/FrameHeader'

export { default as ZstdFrameDecompressor } from './components/zstd/ZstdFrameDecompressor'

export { default as ZstdFrameCompressor } from './components/zstd/ZstdFrameCompressor'

export { default as XxHash64 } from './components/zstd/XxHash64'

/**
 * ar
 */
export { default as ArArchiveEntry } from './components/archivers/ar/ArArchiveEntry'

export { default as ArArchiveInputStream } from './components/archivers/ar/ArArchiveInputStream'

export { default as ArArchiveOutputStream } from './components/archivers/ar/ArArchiveOutputStream'

/**
 * cpio
 */
export { default as CpioArchiveEntry } from './components/archivers/cpio/CpioArchiveEntry'

export { default as CpioArchiveInputStream } from './components/archivers/cpio/CpioArchiveInputStream'

export { default as CpioArchiveOutputStream } from './components/archivers/cpio/CpioArchiveOutputStream'

export { default as CpioConstants } from './components/archivers/cpio/CpioConstants'

export { default as CpioUtil } from './components/archivers/cpio/CpioUtil'

/**
 * dump
 */
export { default as Dirent } from './components/archivers/dump/Dirent'

export { default as DumpArchiveConstants } from './components/archivers/dump/DumpArchiveConstants'

export { default as DumpArchiveEntry } from './components/archivers/dump/DumpArchiveEntry'

export { default as DumpArchiveSummary } from './components/archivers/dump/DumpArchiveSummary'

export { default as DumpArchiveUtil } from './components/archivers/dump/DumpArchiveUtil'

export { default as TapeInputStream } from './components/archivers/dump/TapeInputStream'

export { default as DumpArchiveInputStream } from './components/archivers/dump/DumpArchiveInputStream'

export { default as ByteUtils } from './components/archivers/dump/ByteUtils'

export { TYPE } from './components/archivers/dump/Enumeration'

/**
 * brotli
 */
export { default as BrotliCompressorInputStream
} from './components/compressors/brotli/BrotliCompressorInputStream'

export { default as BrotliUtils } from './components/compressors/brotli/BrotliUtils'

/**
 * bzip2
 */
export { default as BlockSort } from './components/compressors/bzip2/BlockSort'

export { default as BZip2CompressorInputStream
} from './components/compressors/bzip2/BZip2CompressorInputStream'

export { Data, BZip2CompressorOutputStream
} from './components/compressors/bzip2/BZip2CompressorOutputStream'

export { default as BZip2Constants } from './components/compressors/bzip2/BZip2Constants'

export { default as CRC } from './components/compressors/bzip2/CRC'

export { default as Rand } from './components/compressors/bzip2/Rand'

/**
 * deflate
 */
export { DeflateFile, InflateFile } from './components/deflate/deflate'

/**
 * gzip
 */
export { gzipFile, unGzipFile } from './components/gzip/gzip'

//lz4
export { lz4Compressed, lz4Decompressed } from './components/lz4/lz4'

// compressors/lz77
export { default as AbstractLZ77CompressorInputStream
} from './components/compressors/lz77support/AbstractLZ77CompressorInputStream'

export { default as CompressorInputStreams
} from './components/compressors/lz77support/CompressorInputStream'

export { LZ77Compressor, Block, LiteralBlock } from './components/compressors/lz77support/LZ77Compressor'

export { default as Parameters } from './components/compressors/lz77support/Parameters'

// archivers/lzma,sevenz
export { default as Decoder } from './components/archivers/lzma/Decoder'

export { default as Encoder } from './components/archivers/lzma/Encoder'

// compressors/snappy
export { snappyCompress, snappyUncompress } from './components/compressors/snappy/jsSnappy'

// archivers/tar
export { default as TarArchiveEntry } from './components/archivers/tar/TarArchiveEntry'

export { default as TarConstants } from './components/archivers/tar/TarConstants'

export { default as TarArchiveInputStream } from './components/archivers/tar/TarArchiveInputStream'

export { default as TarArchiveOutputStream } from './components/archivers/tar/TarArchiveOutputStream'

//util
export { default as File } from './components/util/File'

export { default as InputStream } from './components/util/InputStream'

export { default as OutputStream } from './components/util/OutputStream'

export { default as System } from './components/util/System'

export { default as IOUtils } from './components/util/IOUtils'

export { default as Exception } from './components/util/Exception'

export { default as ByteArrayInputStream } from './components/util/ByteArrayInputStream'

export { default as ArchiveUtils } from './components/util/ArchiveUtils'

export { default as CharacterSetECI } from './components/util/CharacterSetECI'

export { default as Arrays } from './components/util/Arrays'

export { default as Long } from './components/util/long/index'

export { default as StringBuilder } from './components/util/StringBuilder'

export { default as ByteArrayOutputStream } from './components/util/ByteArrayOutputStream'

// archivers
export { default as ArchiveEntry } from './components/archivers/ArchiveEntry'

export { default as ArchiveInputStream } from './components/archivers/ArchiveInputStream'

export { default as ArchiveOutputStream } from './components/archivers/ArchiveOutputStream'

export { default as ArchiveStreamFactory } from './components/archivers/ArchiveStreamFactory'

// compressors
export { default as CompressorStreamFactory } from './components/compressors/CompressorStreamFactory'

export { default as CompressorInputStream } from './components/compressors/CompressorInputStream'

export { default as CompressorOutputStream } from './components/compressors/CompressorOutputStream'

export { default as CompressorException } from './components/compressors/CompressorException'
